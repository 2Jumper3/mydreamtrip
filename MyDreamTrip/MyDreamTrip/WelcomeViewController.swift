//
//  ViewController.swift
//  MyDreamTrip
//
//  Created by Sergey on 03.06.2020.
//  Copyright © 2020 Sergey. All rights reserved.
//

import UIKit

class WelcomeViewController: UIViewController {
    
    let arImage = UIImageView(image: UIImage(named: "welcome"))
    let splashView  = UIView()

    override func viewDidLoad() {
        self.navigationController?.navigationBar.isHidden = true
        super.viewDidLoad()
        splashView.backgroundColor = .systemPink
        view.addSubview(splashView)
        splashView.frame = CGRect(x: 0, y: 0, width: view.bounds.width, height: view.bounds.height)
        
        arImage.contentMode = .scaleAspectFit
        splashView.addSubview(arImage)
        arImage.frame = CGRect(x: splashView.frame.width/2 - 50, y: splashView.frame.height/2 - 50, width: 100, height: 100)
            
        self.view.backgroundColor = .blue
    }
    override func viewDidAppear(_ animated: Bool) {
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 2.0) {
            self.scaleDownAnimation()
        }
    }
    
    func scaleDownAnimation() {
        UIView.animate(withDuration: 0.5, animations:  {
            self.arImage.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
        }) { (success) in
            self.scaleUpAnimation()
        }
    }
    func scaleUpAnimation() {
        UIView.animate(withDuration: 0.35, delay: 0.1, options: .curveEaseIn, animations: {
            self.arImage.transform = CGAffineTransform(scaleX: 5, y: 5)
        }) { (success) in
            self.removeSplashView()
        }
    }
    func removeSplashView() {
        splashView.removeFromSuperview()
    }
}

